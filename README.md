# Overview

This is the overlay used for some upcoming AGA (American Go Association) broadcasts.

# Getting Started

```git clone``` the repo then run ```npm install```.

```node index.js``` hosts the app on http://localhost:3000.

# Functionality
See the ```config/config.json``` file for parameters. The web page is updated every second with the parameters in this file.
