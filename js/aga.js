window.onload = function() {
    var leftCaptures = 0;
    var rightCaptures = 0;
    var credits_show_button_clicked = false;
    var players_show_button_clicked = false;
    var presentedByLoopStart;
    var headsUpLoopStart;
    var timeoutvar1;
    var timeoutvar2;
    var timeoutvar3;

    socket = io.connect("http://localhost:3000", {'force new connection': true});
    socket.on("connect", function() {
        socket.on("update_overlay", function(overlayInfo) {
            pushOverlayUpdates(overlayInfo);
        })
    })



    document.getElementById("left_minus").onclick = function() {
        leftCaptures -= 1
        captures = document.getElementById("captures")
        captures.innerHTML = leftCaptures + "&nbsp;&nbsp;&nbsp; Captures &nbsp;&nbsp;&nbsp;" + rightCaptures
    }
    document.getElementById("left_plus").onclick = function() {
        leftCaptures += 1
        captures = document.getElementById("captures")
        captures.innerHTML = leftCaptures + "&nbsp;&nbsp;&nbsp; Captures &nbsp;&nbsp;&nbsp;" + rightCaptures
    }
    document.getElementById("right_minus").onclick = function() {
        rightCaptures -= 1
        captures = document.getElementById("captures")
        captures.innerHTML = leftCaptures + "&nbsp;&nbsp;&nbsp; Captures &nbsp;&nbsp;&nbsp;" + rightCaptures
    }
    document.getElementById("right_plus").onclick = function() {
        rightCaptures += 1
        captures = document.getElementById("captures")
        captures.innerHTML = leftCaptures + "&nbsp;&nbsp;&nbsp; Captures &nbsp;&nbsp;&nbsp;" + rightCaptures
    }

    startAnimationLoop();

    $( "#credits_show" ).click(function() {
        if (!credits_show_button_clicked) {
            hideHeadsUp();
            showPresentedBy();
            stopAnimationLoop();
            credits_show_button_clicked = true;

        } else {
            hidePresentedBy();
            credits_show_button_clicked = false;
            startAnimationLoop();
        }

    });

    $( "#player_stats_show" ).click(function() {
        if (!players_show_button_clicked) {
            hidePresentedBy();
            showHeadsUp();
            stopAnimationLoop();
            players_show_button_clicked = true;
        } else {
            hideHeadsUp();
            startAnimationLoop();
            players_show_button_clicked = false;
        }

    });

}

function pushOverlayUpdates(overlayInfo) {
    $('#game_title').html(overlayInfo["game_info"]["game_title"]);
    $('#p1_name').html("&nbsp;&nbsp;&nbsp;&nbsp;" + overlayInfo["game_info"]["p1_name"]);

    // set flags
    $('#p1_flag').attr("src", "./assets/flags/" + overlayInfo["game_info"]["p1_flag"]);
    $('#p2_flag').attr("src", "./assets/flags/" + overlayInfo["game_info"]["p2_flag"]);

    // p1_stone_color, p2_stone_color not trivial
    $('#p1_stone').attr("src", "./assets/stones/" + overlayInfo["game_info"]["p1_stone_color"] + ".png")
    $('#p2_stone').attr("src", "./assets/stones/" + overlayInfo["game_info"]["p2_stone_color"] + ".png")


    $('#p2_name').html(overlayInfo["game_info"]["p2_name"] + "&nbsp;&nbsp;&nbsp;&nbsp;");

    // set p1, p2 photos
    $('#game_title').html(overlayInfo["game_info"]["game_title"]);

    $('#headsup_record').html(overlayInfo["game_info"]["headsup_record"]);

    

    $('#col1_stat1').html(overlayInfo["headsup_info"]["col1_stat1"]);
    $('#col1_stat2').html(overlayInfo["headsup_info"]["col1_stat2"]);
    $('#col1_stat3').html(overlayInfo["headsup_info"]["col1_stat3"]);
    $('#col1_stat4').html(overlayInfo["headsup_info"]["col1_stat4"]);
    $('#col1_stat5').html(overlayInfo["headsup_info"]["col1_stat5"]);

    $('#col2_stat1').html(overlayInfo["headsup_info"]["col2_stat1"]);
    $('#col2_stat2').html(overlayInfo["headsup_info"]["col2_stat2"]);
    $('#col2_stat3').html(overlayInfo["headsup_info"]["col2_stat3"]);
    $('#col2_stat4').html(overlayInfo["headsup_info"]["col2_stat4"]);
    $('#col2_stat5').html(overlayInfo["headsup_info"]["col2_stat5"]);

    $('#col3_stat1').html(overlayInfo["headsup_info"]["col3_stat1"]);
    $('#col3_stat2').html(overlayInfo["headsup_info"]["col3_stat2"]);
    $('#col3_stat3').html(overlayInfo["headsup_info"]["col3_stat3"]);
    $('#col3_stat4').html(overlayInfo["headsup_info"]["col3_stat4"]);
    $('#col3_stat5').html(overlayInfo["headsup_info"]["col3_stat5"]);

    $('#recorder1').html(overlayInfo["presented_by"]["recorder1"]);
    $('#recorder2').html(overlayInfo["presented_by"]["recorder2"]);
    $('#recorder3').html(overlayInfo["presented_by"]["recorder3"]);
    $('#recorder4').html(overlayInfo["presented_by"]["recorder4"]);

    $('#producer1').html(overlayInfo["presented_by"]["producer1"]);
    $('#producer2').html(overlayInfo["presented_by"]["producer2"]);
    $('#producer3').html(overlayInfo["presented_by"]["producer3"]);
    $('#producer4').html(overlayInfo["presented_by"]["producer4"]);

    $('#special_thanks1').html(overlayInfo["presented_by"]["special_thanks1"]);
    $('#special_thanks2').html(overlayInfo["presented_by"]["special_thanks2"]);
    $('#special_thanks3').html(overlayInfo["presented_by"]["special_thanks3"]);
    $('#special_thanks4').html(overlayInfo["presented_by"]["special_thanks4"]);
}

function hideHeadsUp() {
    $( "#players_headsup_overlay" ).fadeTo( "slow" , 0, function() {
        document.getElementById("captures").style.opacity = 1;
    });
}

function showHeadsUp() {
    document.getElementById("captures").style.opacity = 0;
    $( "#players_headsup_overlay" ).fadeTo( "slow" , 1, function() {
        // Animation complete.
    });
}

function hidePresentedBy() {
    $( "#pres_by_overlay" ).fadeTo("slow", 0, function() {
        // Animation complete.
    });
}

function showPresentedBy() {
    $( "#pres_by_overlay" ).fadeTo("slow", 1, function() {
        // Animation complete.
    })
}

function showThenHideHeadsUp() {
    showHeadsUp();
    timeoutvar1 = window.setTimeout(hideHeadsUp, 15000);
}

function showThenHidePresentedBy() {
    showPresentedBy();
    timeoutvar2 = window.setTimeout(hidePresentedBy, 15000);
}

function startPresentedByLoop() {
    presentedByLoopStart = window.setInterval(showThenHideHeadsUp, 360000)
}

function startHeadsUpLoop() {
    headsUpLoopStart = window.setInterval(showThenHidePresentedBy, 360000)
}

function startAnimationLoop() {
    startPresentedByLoop();
    timeoutVar3 = window.setTimeout(startHeadsUpLoop, 180000);
}

function stopAnimationLoop() {
    try{
        clearInterval(presentedByLoopStart);
    } catch (e) {
        null;
    }
    try{
        clearInterval(headsUpLoopStart);
    } catch (e) {
        null;
    }
    try{
        clearInterval(timeoutVar1);
    } catch (e) {
        null;
    }
    try{
        clearInterval(timeoutVar2);
    } catch (e) {
        null;
    }
    try{
        clearInterval(timeoutVar3);
    } catch (e) {
        null;
    }
}
