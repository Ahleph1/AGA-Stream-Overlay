# High Priority - Immediate

Add "thanks" overlay

Handle transitions between screens consistent with times in seconds listed in config/config.json with css animations

Add functionality to existing buttons to bring up specific overlays across the go board with css animations

# High Priority - Very Soon

Finish coloring transition screen elements to the color to be decided on

## Medium Priority - Dev

Get copy of app working on AGA machine

### Low Priority - Dev

Add "as a service" functionality: hosted on a website with login/password so multiple
users can use this at once without each needing to host locally

### Possible Future Direction

Transform into the "meta" version of this app: allow users to add their
own HTML elements (as draggable boxes), timers, and buttons to create their own unique stream overlays
with images they create, outsource the creation of, or find online.
