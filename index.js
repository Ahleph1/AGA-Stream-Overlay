var request = require('request');
var exec = require('child_process').exec;
var http = require('http');
var express = require('express');
var io = require('socket.io')(http);
var path = require('path');
var assert = require('assert');
var fs = require('fs');

var app = express();
app.use(express.static('.'));
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'views', 'index.html'));
});

io.sockets.on('connection', function (socket){
    updateOverlay(socket)
    overlayUpdater = setInterval(updateOverlay, 10000, socket);
});

function updateOverlay(socket) {
    overlayInfo = JSON.parse(fs.readFileSync('config/config.json', 'utf8'));
    socket.emit("update_overlay", overlayInfo);
}

io.listen(app.listen(3000));
